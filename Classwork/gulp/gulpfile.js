// const { src, dest } = require ('gulp');
// const {x,y,z} = 3;
// const z= a.z;
// const y= a.y;
// const {x,y,z,}
const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

function copyHtml() {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('dist/'));

}
// const { watch } = require('gulp');
//
// watch(['input/*.js', '!input/something.js'], function(cb) {
//     // body omitted
//     cb();
// });
function watch() {
    gulp.watch('src/index.html', copyHtml).on('change', browserSync.reload);
    gulp.watch('src/styles/style.scss', sassToCss)
}

function sassToCss() {
    return gulp.src('src/styles/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
}

function serve() {
    browserSync.init({
        server: './dist'
    });
    watch();
}

exports.build = gulp.series(copyHtml, sassToCss);
exports.watch = watch;
exports.sass = sassToCss;
exports.serve = serve;
// const { src, dest } = require('gulp');
//
// function copy() {
//     return src('input/*.js')
//         .pipe(dest('output/'));
// }
//
// exports.copy = copy;