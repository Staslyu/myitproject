function createGame(size = 6) {
    return {
        boxes: [],
        needToClickOn: 1,

        onBoxClick(event) {
        // if(+event.target.innerText === this.needToClickOn);
           if (+event.target.innerText===this.needToClickOn) {
        this.needToClickOn++;
            event.target.style.backgroundColor = "green";
        }
        if(this.needToClickOn === 7) {
            alert('Finished');
            this.boxes.forEach(box => box.style.backgroundColor = "");
            this.boxes.forEach(box => box.removeEventListener('click', null));
        }
        },
        start() {
            this.onBoxClickFn = this.onBoxClick.bind(this);
            this.needToClickOn = 1;
            this.boxes.forEach(box => box.addEventListener('click', event => this.onBoxClick(event)));
        },
        appendTo(container) {
            const gameWrapper = document.createElement('div');
            gameWrapper.classList.add('game');
            this.boxes = Array
                .from({length: size}, (el, i) => i + 1)
                .sort(() => Math.random() - 0.5)
                .map((el) => {
                    const box = document.createElement('div');
                    box.classList.add('box');
                    box.innerText = el;
                    return box;
                });
            this.boxes.forEach(box => gameWrapper.append(box));
            container.append(gameWrapper);
        }
    };
}

const game = createGame();
game.appendTo(document.body);
game.start();