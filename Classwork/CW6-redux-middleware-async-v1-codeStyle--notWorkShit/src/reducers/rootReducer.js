import {combineReducers} from 'redux'
import {fruitsReducer, filmsReducer} from './fruitsReducer';

export const rootReducer = combineReducers({fruits: fruitsReducer, films: filmsReducer});