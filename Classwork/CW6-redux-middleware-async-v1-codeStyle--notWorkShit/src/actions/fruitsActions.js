import {SET_APPLES, SET_BANANAS_REQUEST, SET_BANANAS_SUCCESS} from './constants';


export const setApples = (apples) => {
  return {
    type: SET_APPLES,
    payload: apples
  }
};



export const getMovies = () => {
  return dispach => {
    dispach({
      type: 'GET_MOVIE_REQUEST',
      isFetchingMovie: true
    });
    fetch('https://swapi.co/api/films')
      .then(response => response.json)
      .then(response => {
        dispach({
          type: 'GET_MOVIE_SUCCESS',
          isFetchingMovie: false,
          payload: response.result
        })
      })
  }
};


export const setBananas = (bananas) => {
  return dispatch => {
    dispatch({
      type: SET_BANANAS_REQUEST,
    });

    setTimeout(() => {
      dispatch({
        type: SET_BANANAS_SUCCESS,
        payload: bananas
      })
    }, 1000)
  }
};











