import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMovies, setBananas} from './actions/fruitsActions';

import './App.css'

class App extends Component {

  render() {
    return (
      <div className={'container'}>
        <p>
        Hello World!
        </p>
        { this.props.isFetching ? 'Loading...' : 'Bananas: ' + this.props.bananasInApp}
        <button disabled={this.props.isFetching} onClick={this.handleBananasClick.bind(this)}>
          Set Bananas to {this.props.isFetching ? '...' : this.props.bananasInApp + 1}
        </button>
        <p>

        </p>
      </div>
    );
  }

  handleBananasClick() {
    const increasedCount = this.props.bananasInApp + 1;
    this.props.getMovies();
    this.props.setBananasInApp(increasedCount)
  }
}

const mapStateToProps = state => {
  return {
    bananasInApp: state.fruits.bananas,
    isFetching: state.fruits.isFetching
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setBananasInApp: count => dispatch(setBananas(count)),
    getMovies: () => dispatch(getMovies())
  }
};





export default connect(mapStateToProps, mapDispatchToProps)(App);