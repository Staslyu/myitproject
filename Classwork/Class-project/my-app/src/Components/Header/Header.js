import React from "react";
import Post from "../Content/Post/Post";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

class Header extends React.Component {
    render() {
        return (
            <Router>
                <header className={'header'}>
                    <div className={'logo'}>Logo</div>
                    <nav>
                        <ul className={'nav'}>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/feed">Feed</Link></li>
                            <li><Link to="/post">Post</Link></li>
                        </ul>
                    </nav>
                </header>
                    <Switch>
                        <Route path="/feed">
                            <FeedPage/>
                        </Route>
                        <Route path="/post">
                            <PostPage/>
                        </Route>
                        <Route path="/">
                            <HomePage/>
                        </Route>
                    </Switch>
            </Router>

        );
    }
}

function HomePage() {
    return <h2>Home</h2>;
}

function PostPage() {
    return <Post/>;
}

function FeedPage() {
    return <h2>Feed</h2>;
}

export default Header;