import React from "react";

class Post extends React.Component {

    render() {
        return (
            <div className={'container'}>
                <div className="post">
                    <img className={'post_img'} src="./blog_img.jpeg" alt=""/>
                    <p className={"post_text"}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                        architecto atque autem doloribus, excepturi iste porro similique suscipit temporibus vero.</p>
                </div>
            </div>
        )
    }
}

export default Post;
