import {SET_PHOTOS, SET_TEXT, SET_PHOTOS_REQUEST, SET_TEXT_SUCCESS} from '../actions/constants';


const initialState = {
    photos: '',
    text: 'lalala allalaalla lalalalal lalalaalaldsldl lalal',
    isFetching: false
};

export const blogReducer = (state = initialState, action) => {
    switch (action.type) {
        case (SET_PHOTOS) :
            return {
                ...state,
                photos: action.payload
            };
        case (SET_TEXT) :
            return {
                ...state,
                text: action.payload
            };
        case(SET_PHOTOS_REQUEST) :
            return  {
                ...state,
                isFetching: true
            };

        case (SET_TEXT_SUCCESS):
            return  {
                ...state,
                text: action.payload,
                isFetching: false
            };
        default :
            return state
    }
};
