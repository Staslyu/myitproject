import {SET_PHOTOS, SET_TEXT} from "./constants";

export const setPhotos = (photos) => {
    return {
        type: SET_PHOTOS,
        payload: photos
    }
};

export const setText = (text) => {
    return dispatch => {
        dispatch({
            type: SET_TEXT,
            payload: text
        });
    }
};