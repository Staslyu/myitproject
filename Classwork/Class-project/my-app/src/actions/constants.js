export const SET_PHOTOS = 'SET_PHOTOS';
export const SET_TEXT = 'SET_TEXT';
export const SET_PHOTOS_REQUEST = 'SET_PHOTOS_REQUEST';
export const SET_TEXT_SUCCESS = 'SET_TEXT_SUCCESS';