import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setPhotos, setText} from './actions/blogActions'



import './App.css';
import Header from "./Components/Header/Header";

class App extends Component {
    render() {
        return(
            <div className={'container'}>
                <Header/>
                <p>Hello World!</p>
                {this.props.isFetching ? 'Loading...': 'Photos: '+ this.props.photosInApp}
                <p>{this.props.text}</p>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        photosInApp: state.blog.photos,
        textInApp: state.blog.text,
        isFetching: state.blog.isFetching
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setPhotosInApp: () => dispatch(setPhotos()),
        setTextInApp: () => dispatch(setText())
    }
};





export default connect(mapStateToProps, mapDispatchToProps)(App);