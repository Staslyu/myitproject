export const ownLogger = store => next => action => {
    console.log('OWN LOGGER\n', 'STORE', store.getState(), '\nNEXT_STORE', next, '\nACTION', action);
    return next(action)
};
