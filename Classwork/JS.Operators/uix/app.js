class Appendable {
    _el;

    appendTo(container) {
        container.append(this._el);
    }
}

class ItemForm extends Appendable {
    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        const form = document.createElement('form');
        form.innerHTML = `
        <input placeholder="Name" type="text"/>
                <input placeholder="Description" type="text"/>
        <input placeholder="Price" type="text"/>
          <button>Add</button>
        `;
        this._el = form;
    }
}

class ItemList extends Appendable {
    constructor(data) {
        super();
        this._data = data;
        this._createLayout();
    }

    add(itemData) {
        this._data.push(itemData);
        this._addToLayout(itemData);
    }


    _createLayout() {
        const listEl = document.createElement('ul');
        this._el = listEl;

        this._data.forEach(itemData => {
            const item = new Item(itemData);
        });

        this._data.forEach(this._addToLayout.bind(this));
    }

    appendTo(container) {
        container.append(this._el);
    }

    _addToLayout(itemData) {
        const item = new Item(itemData);
        const itemContainer = document.createElement('li');
        item.appendTo(itemContainer);
        this._el.append(itemContainer);
    }
}


class Item extends Appendable {
    constructor(data) {
        super();
        this._data = data;
        this._createLayout();
    }

    _createLayout() {
        const props = document.createElement('ul');

        let li = document.createElement('li');
        li.innerText = `Name: ${this._data.name}`;
        props.append(li);

        li = document.createElement('li');
        li.innerText = `Description: ${this._data.desc}`;
        props.append(li);

        li = document.createElement('li');
        li.innerText = `Price: ${this._data.price} UAN`;
        props.append(li);

        this._el = props;
    }

    _removeLayout() {

    }

    appendTo(container) {
        container.append(this._el)
    }
}

const itemData = {
    name: 'item name',
    desc: 'description',
    price: 10
};


const list = new ItemList([
    {
        name: 'item name',
        desc: 'description',
        price: 10
    }, {
        name: 'item name',
        desc: 'description',
        price: 10
    }
]);
const form = new ItemForm();
list.appendTo(document.body);
