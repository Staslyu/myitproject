import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore} from "redux";
import {Provider} from 'react-redux'
import * as serviceWorker from './serviceWorker';

import './index.css';

const SET_APPLES = 'SET_APPLES';
const SET_BANANAS = 'SET_BANANAS';

const initialState = {
    apples: 1,
    bananas: 3
};


const fruitsReducer = (state = initialState, action) => {

    switch (action.type) {
        case (SET_APPLES) :

            return {
                ...state,
                apples: action.payload
            };
        case (SET_BANANAS) :

            return {
                ...state,
                bananas: action.payload
            };

        default :
            return state
    }
};

const setApples = (apples) => {
    return {
        type: SET_APPLES,
        payload: apples
    }
};

export const setBananas = (bananas) => {
    return {
        type: SET_BANANAS,
        payload: bananas
    }
};

const store = createStore(fruitsReducer, initialState);

console.log(store.getState());

store.dispatch(setBananas(5));
console.log('after dispatch', store.getState());

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('root')
);
