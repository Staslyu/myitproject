import React, {Component} from 'react';

import logo from './logo.svg';
import './App.css';
import {connect} from "react-redux";
import {setBananas} from "./index";


class App extends Component {
    render() {
        console.log(this.props);

        return (
            <div>
                <p>
                    Hello
                </p>

                <p>
                    Bananas: {this.props.bananasInApp}
                </p>

                <button onClick={() => {
                    this.props.setBananasInApp.bind(this, 7)
                }}>
                    Set Bananas to 7
                </button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        bananasInApp: state.bananas
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setBananasInApp: count => dispatch(setBananas(count))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
