import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';



// function App() {
//
//     const [banana, setBanana] = useState(0);
//     const [apple, setApple] = useState(10);
//
//     useEffect(()=> {
//         console.log(banana)
//     }, [banana]);
//
//
//     return (
//         <div className="App">
//             <div>
//                 <h1>Hi</h1>
//
//                 <p>Banana: {banana}</p>
//                 <p>Apple: {apple}</p>
//                 <button
//                     onClick={() => {
//                     setBanana(banana + 1);
//                 }}>
//                     Banana
//                 </button>
//
//                 <button
//                     onClick={() => {
//                         setApple(apple + 1);
//                     }}>
//                     Apple
//                 </button>
//             </div>
//         </div>
//     );
// }

export default App;
