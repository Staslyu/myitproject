import React from "react";

class Button extends React.Component {
    state = {
        isClicked: false
    };



    render() {
        return (
            <button onClick={() => {
                // this.setState({isClicked: !this.state.isClicked});
                this.setState(prevState => {
                    return {
                        isClicked: !prevState.isClicked
                    }
                });
                console.log('Hi Btn', this.state.isClicked);
            }
            }>
                click</button>
                )
            }
}

    export default Button


// <button onClick={()=> console.log('Hi Btn ')}>click</button>
// <button onClick={console.log.bind(this,'Hi Btn')}>click</button>
