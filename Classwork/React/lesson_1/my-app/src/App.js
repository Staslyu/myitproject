import React from 'react';
import HeaderClass from "./Components/Header/HeaderClass";
import HeaderFunc from "./Components/Header/HeaderFunc";

import './App.css';

class App extends React.Component {

    render() {
        const arr = ['Leonid', 'Sasha', 'Masha', 'Dasha', 'Arra'];

        return (
            <div className="App">
                {arr.map((nameArr, index) => <HeaderClass key={index + nameArr} name={nameArr}/>)}

                {/*<HeaderClass name={'Stas'}/>*/}
                {/*Hello World!*/}
                {/*<HeaderFunc name={'Stas2'}/>*/}
            </div>
        );
    }
}

// function App() {
//     const arr = ['Leonid', 'Sasha', 'Masha', 'Dasha', 'Arra'];
//     return (
//         <div>
//             <HeaderClass name='Stas'/>
//             <HeaderFunc name='Stas2'/>
//             {arr.map((nameArr, index) => <HeaderClass key={index + nameArr} name={nameArr}/>)}
//         </div>
//     )
// }


// function App() {
//     return (
//         <Component name={'Stas'}
//                    name2={'Alex'}
//                    name3={"Masha"}
//         />
//     );
// }
//
// function Component(props) {
//     console.log(props);
//     return (
//         <div>
//             <h1>
//                 Hello {props.name} and {props.name2} {props.name3}!
//             </h1>
//
//             <p>
//                 Lorem ipsum dolor sit amet.
//             </p>
//         </div>
//     )
// }

// class App extends React.Component {
//
//     render() {
//         return <div>dsadsa</div>
//     }
// }

export default App;
