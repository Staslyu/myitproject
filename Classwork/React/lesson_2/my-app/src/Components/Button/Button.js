import React from "react";
import propTypes from 'prop-types';


class Button extends React.Component{
    render() {
        return (
            <button>
                {this.props.text}
                {this.props.value}
            </button>
        )
    }

}

// Button.defaultProps = {
//     text: 'abc',
//     value: 3
// };
//
// Button.propTypes = {
//     text: PropTypes.string,
//     value: PropTypes.number
// };

export default Button;

