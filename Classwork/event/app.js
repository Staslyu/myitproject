// ```
// /*ЗАДАНИЕ - 1
// * По клику по кнопке "Click me" вывести пользователю модальное окно с сообщением "This is click!"
// */

let but = document.createElement("button");
but.innerText = 'Click me';
document.body.append(but);
but.onclick = function (event) {
    let randomColor = [];
    for (let i = 0; i < 3; i++) {
        let colorNumber = Math.floor(Math.random() * 255);
        randomColor.push(colorNumber);
    }
    div1.style.backgroundColor = `rgb(${randomColor.join(',')}`;
};

// but.onmouseover = function (event) {
//     alert('This is mouseover')
// };

let div1 = document.createElement('div');
document.body.append(div1);
div1.style.width = '100px';
div1.style.height = '100px';
div1.style.backgroundColor = 'green';
div1.onclick = function () {
    let randomColor = [];
    for (let i = 0; i < 3; i++) {
        let colorNumber = Math.floor(Math.random() * 255);
        randomColor.push(colorNumber);
    }
    div1.style.backgroundColor = `rgb(${randomColor.join(',')}`;
};

// ​
// /*ЗАДАНИЕ - 2
// * По наведению мышки на кнопку "Click me" вывести пользвоателю окно с сообщением "This is mouseover"
// */
// ​
// /*ЗАДАНИЕ - 3
// * По клику на блок размером 100рх на 100рх менять его цвет на любой(каждый раз новый), кроме прозрачного и белого.*/
// ​
// /*ЗАДАНИЕ - 4
// * Рядом с блоком размером 100рх на 100рх располагается кнопка "изменить цвет" и функция замены цвета блока происходит по нажатию на кнопку, а не на блок.
// */
// ​
// /*ЗАДАНИЕ - 5
// * Рядом с блоком размером 100рх на 100рх располагается поле для ввода цвета.
// * ДОГОВОРЕННОСТЬ - пользователь умеет вводить ТОЛЬКО HEX код цвета. Радом с полем для ввода находится кнопка "Ок".
// * При нажатии на кнопку "Ок" - цвет, введенный пользователем в поле для ввода применяется для блока размером 100рх на 100рх.*
// /
// ​
// /*ЗАДАНИЕ - 6
// * Создать красный круг, размером 80 на 80 пикселей
// * "Привязать" его к курсору мыши, который двигается по странице.
// * Т.е. при загрузке страницы центр красного круга совпадает с положением мыши
// * и при любом перемещении мыши следует за курсором.
// * Добавить "задержку" перемещения красного круга, чтобы создать эффект догонялок
// * */
// ```