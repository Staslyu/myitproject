// let promiseXHR = new Promise((resolve, reject) => {
//     let xhr = new XMLHttpRequest();
//
//     xhr.open('GET', 'https://jsonplaceholder.typicode.com/todos/1');
//     xhr.onloadend = () => resolve(xhr.response);
//     xhr.onerror = () => reject(xhr.error);
//
//     xhr.send();
// });
//
// let promise = new Promise((resolve, reject) => {
//     setTimeout(() => resolve('abcdfwfdsfsdffdkfsjsdkcnvxmcvnxmkk'), 1000)
// });
//
// promiseXHR
//     .then((result) => console.log('Resolve in Promise = ', result))
//     .then((result) => console.log('Catch Last Line : Reject = ', result))
//     .catch((error) => console.log('First then: Reject - Error = ', error));
//
// fetch('https://jsonplaceholder.typicode.com/todos/1')
//     .then((result) => {
//         console.log('result = ', result);
//         result.json()
//     })
//     .then(
//         (result2) => console.log('result2 = ', result2));

async function getData() {
    let result = await fetch('https://jsonplaceholder.typicode.com/todos/1');
    console.log('result', result);
    let resultResult = await result.json();
    return result
}

getData().then(console.log);