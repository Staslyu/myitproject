const dwsForm = document.getElementsByClassName("section-third-menu");
const arrDwsForm = [...dwsForm];
const tabs = document.getElementsByClassName("our-services-menu");
const arrTabs = [...tabs];
const tabForms = document.getElementsByClassName("third-content");
const arrTabForms = [...tabForms];
for(let i = 0; i < arrTabs.length; i++){
    arrTabs[i].addEventListener("click", function(){
        for(let i = 0; i < arrTabs.length; i++){
            arrTabs[i].classList.remove("active");
            arrTabForms[i].classList.remove("active");
        }
        this.classList.add("active");
        arrTabForms[arrTabs.indexOf(this)].classList.add("active");
        console.log(arrTabs.indexOf(this));
    });
}

$(document).ready(function() {
$(".loadMoreSecond").hide(10);
    $(".moreBox").slice(0, 3).hide();
    $(".lds-ellipsis").hide();
    if ($(".blogBox:hidden").length != 0) {
        $(".loadMore").show();
    }

    $(".loadMore").on('click', function (e) {
        $(".lds-ellipsis").fadeIn('slow');
        $(".lds-ellipsis").hide(2000);
        e.preventDefault();
        $(".moreBox:hidden").slice(0, 6).slideDown();
        if ($(".moreBox:hidden").length == 0) {
            $(".loadMore").fadeOut('fast');
            $(".loadMoreSecond").fadeIn('fast');
        }

});
            $('.loadMoreSecond').on('click', function () {
                if ($(".moreBox:hidden").length == 0) {
                    $(".lds-ellipsis").fadeIn('fast');
                    $(".lds-ellipsis").fadeOut(2000);
                }
            });
    });

$(document).ready(function() {
    $(".moreBoxSecond").slice(0, 3).hide();
    if ($(".blogBoxSecond:hidden").length != 0) {
        $(".loadMoreSecond").show();
    }
    $(".loadMoreSecond").on('click', function(e) {
        e.preventDefault();
        $(".moreBoxSecond:hidden").slice(0, 6).slideDown();
        if ($(".moreBoxSecond:hidden").length == 0) {
            $(".loadMoreSecond").fadeOut('slow');
        }
    });
});


setTimeout(function() {
    $('#lds-ellipsis').hide();
}, 3000);

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
}


filterSelection("all");
function filterSelection(c) {
    let x, i;
    x = document.getElementsByClassName("img");
    if (c == "all") c = "";
    for (i = 0; i < x.length; i++) {
        w3RemoveClass(x[i], "show");
        if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
    }
}

function w3AddClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) == -1) {
            element.className += " " + arr2[i];
        }
    }
}

function w3RemoveClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}

const btnContainer = document.getElementsByClassName("picture-menu");
const btns = document.getElementsByClassName("li-img-box");
for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function(){
        let current = document.getElementsByClassName("active");
        current[6].className = current[6].className.replace(" active", "");
        this.className += " active";
    });
}

// $('.grid').masonry({
//     itemSelector: '.grid-item',
//     columnWidth: 370,
//     originLeft: false,
//     gutter: 0
// });
