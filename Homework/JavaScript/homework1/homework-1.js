// Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// Почему объявлять переменную через var считается плохим тоном?
//
// 1. Разная область видимость для JS. У var область видимости глобальная let очень похоже на var, основное отличие — ограниченная
// область видимости переменных, объявляемых с его помощью. В const записывается переменная, которая не будет изменена в дальнейшем.
//
// 2. Если объявить переменную с использованием var, то обратиться к ней в функции можно и после выхода из той конструкции, где она была объявлена.
//    Область видимости переменных, объявленных с его использованием, оказывается слишком большой. Это может привести к непреднамеренной перезаписи
// данных и к другим ошибкам. Большие области видимости часто ведут к появлению неаккуратных программ.

    let name = prompt('Enter your name');

while (name === ''){
   name = prompt('Enter your name');
}

let age = +prompt('Enter your age');

while (isNaN(age) || age == false){
    age = +prompt('Enter your age');
}

if (age < 18) {
    alert('You are not allowed to visit this website')
} else if (age >= 18 && age <= 22) {
    let answer = confirm('Are you sure you want to continue?');
    if (answer) {
        alert(`Welcome, ${name}`)
    } else {
        alert('You are not allowed to visit this website')
    }
} else {
    alert('Welcome')
}
