//Метод forEach() выполняет указанную функцию один раз для каждого элемента в массиве. В основном используется для перебора элементов массива

   const firstArgument = ['water', 'milk', 15, '24', null, 'Anton'];
   function filterBy(initArray, filterType) {
      return initArray.filter(el => typeof el !== filterType)
   }
   console.log(filterBy(firstArgument, 'string'));
   console.log(firstArgument);