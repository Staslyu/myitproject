// События - это то, что происходит при взаимодействии пользователя. Обработчик события это то, что происходит при взаимодействии пользователя.

const createText = document.createElement('p');
createText.innerText = 'Price';
document.body.append(createText);
const createInput = document.createElement('input');
document.body.append(createInput);
const createP = document.createElement('p');
createInput.placeholder = 'Enter a price';
createText.style.display = 'inline-block';
createInput.onfocus = function () {
  createInput.style.border = '1px solid green';
};


createInput.addEventListener('focusout', function () {
if (createInput.value < 0) {
  createInput.style.border = '1px solid red';
  document.body.after(createP);
  createP.innerHTML = 'Please enter correct price';
}

else {
  createP.remove();
  const createSpan = document.createElement('span');
  document.body.append(createSpan);
  createInput.style.border = 'none';
  createSpan.innerHTML = 'Текущая цена:$ '+ +createInput.value;
  const close = document.createElement('button');
  document.body.append(close);
  close.style.cssText = `
  opacity:0,3;
  background-image: url("closeX.png")`;
  close.onclick = function f() {
    createSpan.remove();
    close.remove();
    createInput.value = '';
  }
}});

