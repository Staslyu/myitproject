/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

class Hamburger {
    constructor(size, stuffing) {
        if(size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE) {
            this._size = size;
        } else if (size === undefined) {
            throw new HamburgerException('No size given');
        } else if (size !== Hamburger.SIZE_SMALL || Hamburger.SIZE_LARGE){
            throw new HamburgerException('HamburgerException: invalid size' + ' ' + size.name);
        }
        if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO) {
            this._stuffing = stuffing;
        } else if (stuffing === undefined) {
            throw new HamburgerException('no stuffing given!');
        } else if (stuffing !== Hamburger.STUFFING_CHEESE || stuffing !== Hamburger.STUFFING_SALAD || stuffing !== Hamburger.STUFFING_POTATO){
            throw new HamburgerException('invalid stuff' + stuffing.name);
        }

        this._toppings = [];

    }

    /**
     * Добавить добавку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     *
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */

    addTopping(topping) {
        if (!this._toppings.includes(topping)) {
            if (topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE) {
                this._toppings.push(topping);
            } else {
                throw new HamburgerException("topping name is incorrect or not given");
            }
        } else {
            throw new HamburgerException("you already put this topping");
        }
    }

    /**
     * Убрать добавку, при условии, что она ранее была
     * добавлена.
     *
     * @param topping   Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */

    removeTopping(topping) {
        if (topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE) {
            this._toppings = this._toppings.filter(x => x !== topping);
        } else {
            throw new HamburgerException("removing-topping name is incorrect or not given");
        }
    }


    /**
     * Получить список добавок.
     *
     * @return {Array} Массив добавленных добавок, содержит константы
     *                 Hamburger.TOPPING_*
     */

    get getToppings() {
        return this._toppings;
    }

    /**
     * Узнать размер гамбургера
     */

    get getSize() {
        return this._size;
    }
    /**
     * Узнать начинку гамбургера
     */

    get getStuffing() {
        return this._stuffing;
    }

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена в тугриках
     */


    get calculatePrice() {
        const priceArr = this._toppings.map(x => Hamburger.TOPPINGS[x].price);
        priceArr.push(Hamburger.SIZES[this._size].price, Hamburger.STUFFINGS[this._stuffing].price);
        let price = priceArr.reduce((acc, prices) => acc + prices, 0);
        return price;
    }

    /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */

    get calculateCalories() {
        const caloriesArr = this._toppings.map(x => Hamburger.TOPPINGS[x].calories);
        caloriesArr.push(Hamburger.SIZES[this._size].calories, Hamburger.STUFFINGS[this._stuffing].calories);
        let calories = caloriesArr.reduce((acc, itemcalories) => acc + itemcalories, 0);
        return calories;
    }
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

function HamburgerException(message)  {
    this.name = "HamburgerException";
    this.message = message;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, HamburgerException);
    } else {
        this.stack = (new Error()).stack;
    }
}

HamburgerException.prototype = Object.create(Error.prototype);

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';

Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL]: {
        price: 30,
        calories: 50,
    },
    [Hamburger.SIZE_LARGE]: {
        price: 50,
        calories: 100,
    },
};

Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = "STUFFING_SALAD";
Hamburger.STUFFING_POTATO = "STUFFING_POTATO";

Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE]: {
        price: 15,
        calories: 20,
    },
    [Hamburger.STUFFING_SALAD]: {
        price: 20,
        calories: 5,
    },
    [Hamburger.STUFFING_POTATO]: {
        price: 35,
        calories: 15,
    },
};

Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.TOPPING_MAYO = "TOPPING_MAYO";

Hamburger.TOPPINGS = {
    [Hamburger.TOPPING_SPICE]: {
        price: 10,
        calories: 0,
    },
    [Hamburger.TOPPING_MAYO]: {
        price: 15,
        calories: 5,
    },
};


const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

hamburger.addTopping(Hamburger.TOPPING_SPICE);

console.log("Stuffing:", hamburger.getStuffing);

console.log("Calories: ", hamburger.calculateCalories);

console.log("Price: ", hamburger.calculatePrice);

hamburger.addTopping(Hamburger.TOPPING_MAYO);

console.log("Price with sauce: ", hamburger.calculatePrice);

console.log("Is hamburger large: ", hamburger.getSize === Hamburger.SIZE_LARGE);

hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log("Hamburger has %d toppings", hamburger.getToppings.length);

console.log("ИТОГО :");
console.log("finalToppingsList", hamburger._toppings);
console.log(" finalPrice: ", hamburger.calculatePrice);
console.log(" finalCalories: ", hamburger.calculateCalories);