function createTable(cols, rows) {
    let body = document.getElementsByTagName('body')[0];
    let table = document.createElement('table');
    table.className = 'table';
    for (let i = 0; i < rows; i++) {
        let tableRow = document.createElement('tr');
        tableRow.className = "row";
        for (let j = 0; j < cols; j++) {
            let tableColumn = document.createElement('td');
            tableColumn.className = 'column';
            tableRow.appendChild(tableColumn);
        }
        table.appendChild(tableRow);
    }
    document.body.appendChild(table);

    let changeColor = (e)=>{
        if( e.target.tagName === "TD"){
            e.target.classList.toggle("column-active");
        }
        else if (e.target.tagName === "BODY") {

            let tdMy = document.getElementsByClassName('column');
            for (let i = 0; i<tdMy.length; i++){
                tdMy[i].classList.toggle("column-active");
            }
        }
    };
    body.addEventListener('click',changeColor);
}
createTable(30, 30);