// /* Класс, объекты которого описывают параметры гамбургера.
// *
// * @constructor
// * @param size        Размер
// * @param stuffing    Начинка
// * @throws {HamburgerException}  При неправильном использовании
// */
function Hamburger(size, stuffing) {
    try {
        if (size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE) {
            var burgerSize = size;
            this.getSize = function () {
                return burgerSize;
            };
        } else if (size === undefined) {
            throw new HamburgerException('Invalid size.');

        } else if (size !== Hamburger.SIZE_SMALL || Hamburger.SIZE_LARGE) {
            throw new HamburgerException('No SIZE given.');
        }

        if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO) {
            var burgerStuffing = stuffing;
            this.getStuffing = function () {

                return burgerStuffing
            };
        } else if (stuffing === undefined) {
            throw new HamburgerException('Invalid STAFFING.');
        } else if (stuffing !== Hamburger.STUFFING_CHEESE || Hamburger.STUFFING_SALAD || Hamburger.STUFFING_POTATO) {
            throw new HamburgerException('No STAFFING given.');
        }

        var burgerToppings = [];

        this.getBurgerToppings = function () {
            return burgerToppings;
        };
        this.setBurgerToppings = function (value) {
            return burgerToppings = value;
        }
    } catch(e) {
        console.log(e.message);
    }
}

// /* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10
};
Hamburger.ALL_TOPPINGS = {
    TOPPING_MAYO: {
        name: 'TOPPING_MAYO',
        price: 20,
        calories: 5
    },
    TOPPING_SPICE : {
        name: 'TOPPING_SPICE',
        price: 15,
        calories: 0
    }
};
// /* Добавить добавку к гамбургеру. Можно добавить несколько
// * добавок, при условии, что они разные.
// *
// * @param topping     Тип добавки
// * @throws {HamburgerException}  При неправильном использовании
// */
Hamburger.prototype.addTopping = function (topping) {
    if (!this.getBurgerToppings().includes(topping)) {
        if (topping === Hamburger.ALL_TOPPINGS.TOPPING_MAYO || topping === Hamburger.ALL_TOPPINGS.TOPPING_SPICE) {
            this.getBurgerToppings().push(topping);
        } else {
            throw new HamburgerException("topping name is incorrect or not given");
        }
    } else {
        throw new HamburgerException("you already put this topping");
    }
};
// /*
//  * Убрать добавку, при условии, что она ранее была
//  * добавлена.
//  *
//  * @param topping   Тип добавки
//  * @throws {HamburgerException}  При неправильном использовании  */
Hamburger.prototype.removeTopping = function (topping) {
    if (topping === Hamburger.ALL_TOPPINGS.TOPPING_MAYO || topping === Hamburger.ALL_TOPPINGS.TOPPING_SPICE) {
        var filteredToppings = this.getBurgerToppings().filter(function (toppingType) {
            return toppingType !== topping;
        });
    } else {
        throw new HamburgerException("removing-topping name is incorrect or not given");
    }
    return this.setBurgerToppings(filteredToppings);
};

// /* Получить список добавок.
// *
// * @return {Array} Массив добавленных добавок, содержит константы
// *                 Hamburger.TOPPING_*
// */

Hamburger.prototype.getToppings = function () {
    return this.getBurgerToppings();
};
//
//
// /* Узнать размер гамбургера
// */
Hamburger.prototype.getSize = function () {
    return this.getSize();
};

//
//
//
// /* Узнать начинку гамбургера
// */
Hamburger.prototype.getStuffing = function () {
    return this.getStuffing();
};
//
//
//
//
// /* Узнать цену гамбургера
// * @return {Number} Цена в тугриках
// */
Hamburger.prototype.calculatePrice = function () {
    return (this.getBurgerToppings().map(function(x){return x.price})).reduce(function(acc, prices) { return acc + prices}, 0)
        + this.getSize().price + this.getStuffing().price
};

//
// /* Узнать калорийность
// * @return {Number} Калорийность в калориях
// */
Hamburger.prototype.calculateCallories = function () {
    return (this.getBurgerToppings().map(function(x){ return x.calories})).reduce( function(acc, prices) {return acc + prices}, 0)
        + this.getSize().calories + this.getStuffing().calories
};
//
// /* Представляет информацию об ошибке в ходе работы с гамбургером.
// * Подробности хранятся в свойстве message.
// * @constructor
// */
function HamburgerException(message)  {
    this.name = "HamburgerException";
    this.message = message;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, HamburgerException);
    } else {
        this.stack = (new Error()).stack;
    }
}

HamburgerException.prototype = Object.create(Error.prototype);

var hamburger = new Hamburger(Hamburger.SIZE_SMsALL, Hamburger.STUFFING_SALAD);

console.log(hamburger.getSize());
console.log(hamburger.getStuffing());
hamburger.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO);
hamburger.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE);
console.log(hamburger.getToppings());

console.log(hamburger.getToppings());
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCallories());
console.log(hamburger.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO));
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCallories());