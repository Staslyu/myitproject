
    let dogsFavor = [{
    name: "Мария",
    lastName: "Салтыкова",
    age: 25
}, {
    name: "Осана",
    lastName: "Меньшинова",
    age: 20
}, {
    name: "Андрей",
    lastName: "Первозваный",
    age: 100
}, {
    name: "Василий",
    lastName: "Гофман",
    age: 40
}, {
    name: "Поручик",
    lastName: "Ржевский",
    age: "вечно молодой"
}];

    let catsFavor = [{
    name: "Мария",
    lastName: "Розгозина",
    age: 22
}, {
    name: "Осана",
    lastName: "Меньшинова",
    age: 20
}, {
    name: "Андрей",
    lastName: "Первозваный",
    age: 100
}, {
    name: "Алексей",
    lastName: "Гофман",
    age: 40
}, {
    name: "Капитан",
    lastName: "Очевидность",
    age: "вечно молодой"
}];

    const yFilter = catsFavor.map(itemCats => { return itemCats.name+itemCats.lastName; });
    const peopleInHeaven = dogsFavor.filter(itemDogs => !yFilter.includes(itemDogs.name+itemDogs.lastName));
    console.log(peopleInHeaven);



//     <!--  У вас есть массив объектов (женихи из Японии), каждый объект - характеристики потенциального жениха.. Создайте из  них другой массив -  "завидные женихи". Завидным считается парень с ростом  выше 170 см или с зарплатой больше 100 000 долларов в год.
// Использование стрелочных функций приветствуется.
//   -->


        const japanMen = [{name: "Акэти Мицихура", height: 180, salary: 20000}, {name: "Санада Юкимура", height: 169, salary: 11000}, {name: "Ода Нобунага", height: 165, salary: 200000}, {name: "Уэсуги Кэнсин", height: 160, salary: 25000}, {name: "Такэда Сингэн", height: 165, salary: 80000}, {name: "Мори Мотонари", height: 185, salary: 75000}, {name: "Датэ Масамунэ", height: 180, salary: 750000}];

    const japanBestMen = japanMen.filter(japanBestMen => {
        return japanBestMen.height > 170 || japanBestMen.salary > 100000;
    });
    console.log(japanBestMen);

    /*В результате у вас должен повится новый массив japanBestMen с такими значениями:

    const japanBestMen = [{name: "Акэти Мицихура", height: 180, salary: 20000}, {name: "Ода Нобунага", height: 165, salary: 200000}, {name: "Мори Мотонари", height: 185, salary: 75000}, {name: "Датэ Масамунэ", height: 180, salary: 750000}];
    */






    // {/*<!--   У вас есть массив книг books. Ваша задача создать из него два массива - goodBooks и badBooks. В первый входят все книни, автор которых - не Донцова. Во второй - книги, автор которых - Донуцова. Потому что:*/}
    // {/*http://cdn01.ru/files/users/images/15/5c/155c1d353f38ac999440fee98d912f45.jpg*/}
    // {/*-->*/}

    let books = [ {author: "Скотт Бэккер", book: "Князь пустоты"}, {author: "Донцова", book: "Старуха Кристи – отдыхает!"}, {author: "Скотт Бэккер", book: "Тьма, что приходит прежде"}, {author: "Скотт Бэккер", book: "Воин-пророк"}, {author: "Скотт Бэккер", book: "Тысячекратная мысль"}, {author: "Донцова", book: "Диета для трех поросят"}, {author: "Скотт Бэккер", book: "Аспект-император"}, {author: "Скотт Бэккер", book: "Око Судии"}, {author: "Скотт Бэккер", book: "Воин Доброй удачи"}, {author: "Донцова", book: "Идеальное тело Пятачка"}, {author: "Скотт Бэккер", book: "Великая Ордалия"}, {author: "Скотт Бэккер", book: "Нечестивый Консульт"}, {author: "Скотт Бэккер", book: "Не-Бог"}, {author: "Скотт Бэккер", book: "Истории о Злодеяниях"}, {author: "Донцова", book: "Идеальное тело Пятачка"}, {author: "Скотт Бэккер", book: "Ложное солнце"}, {author: "Донцова", book: "Дед Снегур и Морозочка"}, {author: "Скотт Бэккер", book: "Четыре Откровения Киниал’джина"}, {author: "Донцова", book: "Инь, янь и всякая дрянь"}, {author: "Скотт Бэккер", book: "Нож, что всем по руке"} ];

    function filterByValue(array, string) {
        return array.filter(o =>
            Object.keys(o).some(k => o[k].toLowerCase().includes(string.toLowerCase())));
    }
const badBooks = filterByValue(books, 'Донцова');
    console.log(badBooks);
const goodBooks = filterByValue(books, 'Скотт Бэккер');
    console.log(goodBooks);





    //
//     <!--   У вас есть массив строк, каждая строка - один продукт, который входит в состав английского завтрака. Напишите код, который работает так: если в состав завтрака входит больше 4х продуктов, то остальные переносятся на обед.
// -->


        const englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы", "жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];

    // let myArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    // let dinner = split(myArr, v => (v.length < 5));
    // console.log(dinner);
    if (englishBreakfast.length>4) {
        dinner=englishBreakfast.slice(4,englishBreakfast.length)
    }
    console.log(dinner);
    /* в результате массив englishBreakfast должен быть равен:
    englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы"];
    и должен появится еще массив dinner, со значениями:
    dinner = ["жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];
    */




  //
  //   <!-- Представьте себе ситуацию - вам нужно оценить, является ли комментарий пользователя на сайте недопустимым с лексической точки зрения (то есть содержит слова, которые нельзя употреблят, например: “огромные выигрыши в нашем казино набирай в браузере …. ”). Логично, что для этого нужно проверить содержит ли комментарий пользователя на сайте слишком много спам-слов (например, 1-2 слова “бесплатно” - это ок, но 3 и больше уже явный признак спама ). Для этого вам нужно написать функцию (именно функцию) isSpam, которая будет принимать 3 аргумента:
  //  - строку, которую ввел пользователь;
  //  - слово, переизбыток которого считается спамом (например, “выигрыш”);
  //  - количество повторений слова, которое считается спамом (например, 3 - то есть если слово “выигрыш” встречается в строке 1-2 раза, это не спам, а если 3 и больше - спам).
  //  И возвращать функция isSpam должна true, если в переданной строке спам-слово встречается указанное или больше число раз, и false - если нет.
  //  При этом и комментарий, и спм-слово, нужно ввести в поля ввода, которые написаны ниже (input) и повесить выполнение функции на клик на кнопке “Проверить на спам”. Результат выполнения функции вывести в p с id spam-check-result.
  //
  // -->


    function Click(){
        document.getElementById("spam-check-result").innerHTML=checkSpam(document.getElementById("comment").value,document.getElementById("spam-word").value,3);
    }

    function checkSpam(str,wrd,nmb){
        let digger = -1;
        let count=0;
        while ((digger = str.indexOf(wrd, digger + 1)) != -1) {
            count++;
        }
        if(count<nmb){  return false} else { return true}
    }
